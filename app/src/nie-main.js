import Vue from "vue";
import App from "./App.vue";
// import App from "./App-test.vue";
import BaseCard from "./components/UI/BaseCard.vue";
import BaseDialog from "./components/UI/BaseDialog.vue";
import router from "./router";
import { store } from "./store/store";

import authentication from "@/plugins/authentication"; // plugin do zabezpieczania poszczegolnych routow

Vue.config.productionTip = true;

Vue.component("base-card", BaseCard);
Vue.component("base-dialog", BaseDialog);

// bez keycloka żadnego, normalne zamontowanie appki bez logowania
// new Vue({
//   store,
//   router,
//   render: (h) => h(App),
// }).$mount("#app");

Vue.use(authentication);

Vue.$keycloak.init({ checkLoginIframe: false }).then(() => {
  new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount("#app");
});
