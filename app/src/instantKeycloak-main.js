import Vue from "vue";
import App from "./App.vue";
// import App from "./App-test.vue";
import BaseCard from "./components/UI/BaseCard.vue";
import BaseDialog from "./components/UI/BaseDialog.vue";
import router from "./router";
import { store } from "./store/store";
// import VueNativeSock from "vue-native-websocket";

import VueLogger from "vuejs-logger";
import * as Keycloak from "keycloak-js";

Vue.config.productionTip = true;

// Vue.use(VueNativeSock, "wss://fantastic-app.mac.pl:8080");
Vue.component("base-card", BaseCard);
Vue.component("base-dialog", BaseDialog);

Vue.use(VueLogger);

let initOptions = {
  url: "https://fantastic-keycloak.mac.pl:8080/auth/",
  realm: "test",
  clientId: "fantastic-app",
  onLoad: "login-required",
  "ssl-required": "external",
  resource: "fantastic-app",
  "public-client": true,
  "confidential-port": 0,
};
let keycloak = Keycloak(initOptions);

keycloak
  .init({ onLoad: initOptions.onLoad })
  .then((auth) => {
    if (!auth) {
      window.location.reload();
    } else {
      Vue.$log.info("Authenticated");

      new Vue({
        el: "#app",
        store,
        router,
        render: (h) => h(App, { props: { keycloak: keycloak } }),
      });
    }

    //Token Refresh
    setInterval(() => {
      keycloak
        .updateToken(70)
        .then((refreshed) => {
          if (refreshed) {
            Vue.$log.info("Token refreshed" + refreshed);
          } else {
            // Vue.$log.warn(
            //   "Token not refreshed, valid for " +
            //     Math.round(
            //       keycloak.tokenParsed.exp +
            //         keycloak.timeSkew -
            //         new Date().getTime() / 1000
            //     ) +
            //     " seconds"
            // );
          }
        })
        .catch(() => {
          Vue.$log.error("Failed to refresh token");
        });
    }, 6000);
  })
  .catch(() => {
    Vue.$log.error("Authenticated Failed");
  });
// keycloak.updateToken(-1);

// new Vue({
//   store,
//   router,
//   render: (h) => h(App, { props: { keycloak: keycloak } }),
// }).$mount("#app");
