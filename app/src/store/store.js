import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

const BASE_API_URL = process.env.VUE_APP_BASE_API_URL;

export const store = new Vuex.Store({
  state: {
    storedResources: [],
    keycloakToken: null,
  },

  getters: {
    getStoreResource(state) {
      // console.log("vuex getter: ", state.storedResources);
      return state.storedResources;
    },
    getKeycloakToken(state) {
      return state.keycloakToken;
    },
  },

  mutations: {
    TOKEN_SET(state, token) {
      state.keycloakToken = token;
    },
    allResourcesFromDb(state, data) {
      state.storedResources = data.reverse();
    },
  },

  actions: {
    getAllResources: (context) => {
      axios
        .get(`${BASE_API_URL}/allResources`)
        .then((res) => {
          console.log("context co ma?: ", context);
          console.log("actions getAllResources res.data: ", res.data);
          context.commit("allResourcesFromDb", res.data);
        })
        .catch((error) => console.log("error getAll: ", error));
    },

    addOneResource: (context, payload) => {
      console.log("payload: ", payload);
      axios
        .post(`${BASE_API_URL}/resource`, payload)
        .then((res) => {
          console.log(res);
          context.dispatch("getAllResources");
        })
        .catch((err) => {
          console.log("error in axios post: ", err);
        });
    },

    deleteOneResource: (context, payload) => {
      axios
        .delete(`${BASE_API_URL}/resource/${payload}`)
        .then((res) => {
          console.log(res);
          context.dispatch("getAllResources");
        })
        .catch((err) => {
          console.log("error in axios delete: ", err);
        });
    },
    editOneResource: (context, payload) => {
      console.log("payload in editOne: ", payload);
      const updatedResource = {
        title: payload.title,
        description: payload.description,
        link: payload.link,
      };
      axios
        .patch(`${BASE_API_URL}/resource/${payload.resId}`, updatedResource)
        .then((res) => {
          console.log(res);
          context.dispatch("getAllResources");
        })
        .catch((err) => {
          console.log("error in axios patch: ", err);
        });
    },
  },
});
