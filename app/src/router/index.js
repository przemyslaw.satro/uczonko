import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../components/home/Home.vue";
import KeycloakOptions from "../components/keycloak-options/KeycloakOptions.vue";
// import WisielecTest from "../components/wisielec/WisielecTest.vue";
import ToDoApp from "../components/first-todo/ToDoApp.vue";
import TheResources from "../components/stored-resources/TheResources.vue";
import TheResourcesVuex from "../components/stored-resources-with-vuex/TheResources.vue";
import TheClock from "../components/clock/TheClock.vue";
import WeatherApp from "../components/check-weather/Weather.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "TheHome",
    component: Home,
    meta: {
      isAuthenticated: false,
    },
  },
  {
    path: "/checkWeather",
    name: "WeatherApp",
    component: WeatherApp,
  },
  {
    path: "/show-keycloak-obect",
    name: "KeycloakOptions",
    component: KeycloakOptions,
  },
  // {
  // path: '/about',
  // name: 'about',
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  // component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // },
  {
    path: "/wisielec",
    name: "wisielec",
    meta: {
      isAuthenticated: false,
    },
    component: () => import("../components/wisielec/WisielecTest.vue"),
  },
  {
    path: "/first-ToDo",
    name: "first-todo",
    component: ToDoApp,
  },
  {
    path: "/uczonko",
    name: "uczonko",
    component: TheResources,
  },
  {
    path: "/uczonko-vuex",
    name: "uczonko-vuex",
    component: TheResourcesVuex,
  },
  {
    path: "/clock",
    name: "TheClock",
    component: TheClock,
  },
  {
    path: "*",
    redirect: "/",
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
