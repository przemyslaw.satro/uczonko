// vue.config.js
const fs = require("fs");
const path = require("path");
var webpack = require("webpack");

let disableHostCheck;
let https;

const opts = {
  DEBUG: false,
  IS_WEB: true,
  // IS_WEB: process.env.VUE_APP_IS_WEB,
  ENGINE_RUN: process.env.VUE_APP_ENGINE_RUN,
  version: 1,
  // add this for verbose output
  "ifdef-verbose": false,
  // add this to use double slash comment instead of default triple slash
  "ifdef-triple-slash": true,
};

const optImg = {
  // name: 'static/media/audio/[folder]/[name].[ext]',
  name: "static/img/[name].[hash:8].[ext]",
  context: "static",
};

const opt = {
  limit: -1,
  name: "static/[path]/[name].[ext]",
  context: "static",
};
disableHostCheck = true;
if (process.env.NODE_ENV !== "production") {
  https = {
    key: fs.readFileSync("ssl/tls.key"),
    cert: fs.readFileSync("ssl/tls.crt"),
  };
}

module.exports = {
  // options...
  runtimeCompiler: true,
  assetsDir: "./static",
  chainWebpack: (config) => {
    config.resolve.alias
      .set("__staticDir", path.join(__dirname, "./static"))
      .set("__mainSrcDir", path.join(__dirname, "./src"))
      .set(
        "__multibookTypeDir",
        path.join(
          __dirname,
          "./static",
          `./${process.env.VUE_APP_MULTIBOOK_TYPE}`
        )
      )
      .set("__rootDir", path.join(__dirname));

    config.module
      .rule("js")
      .use("ifdef-loader")
      .loader("ifdef-loader")
      .tap(() => {
        // modify the options...
        return opts;
      })
      .end();
    config.module
      .rule("images")
      .test(/\.(png|jpe?g|gif|webp)(\?.*)?$/)
      .exclude.add(/3d/)
      .end()
      .use("url-loader")
      .loader("file-loader")
      .tap(() => {
        return optImg;
      })
      .end();
    config.module
      .rule("media")
      .test(/\.(ogg|mp3|wav|flac|aac|wma|mp4|webm)(\?.*)?$/)
      .use("url-loader")
      .loader("file-loader")
      .tap(() => {
        return opt;
      })
      .end();
    config.module
      .rule("3d")
      .test(/\.(obj|mtl|png|jpe?g|gif)(\?.*)?$/)
      .include.add(/3d/)
      .end()
      .use("url-loader")
      .loader("file-loader")
      .tap(() => {
        return opt;
      })
      .end();
  },
  devServer: {
    compress: true,
    public: process.env.VUE_APP_BASE_URL,
    disableHostCheck: disableHostCheck,
    https: https,
  },
};
