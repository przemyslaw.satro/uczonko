import Vue from "vue";
import App from "./App.vue";
// import App from "./App-test.vue";
import BaseCard from "./components/UI/BaseCard.vue";
import BaseDialog from "./components/UI/BaseDialog.vue";
// import LogOutBtn from "./components/UI/LogOutBtn.vue";

import router from "./router";
import { store } from "./store/store";
Vue.component("base-card", BaseCard);
Vue.component("base-dialog", BaseDialog);
// Vue.component("log0out-btn", LogOutBtn);

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
