const express = require("express");
const https = require("https");
const http = require("http");
const fs = require("fs");
const path = require("path");
// const cors = require("cors");  // niepotrzebne bo mam swój middleware
const { connectToDb, getDb } = require("./client");

const HTTP_PORT = 4999;
const HTTPS_PORT = 5000;
const httpApp = express();
const httpsApp = express();

httpsApp.use(express.json());
// httpsApp.use(cors());            // wszystkie corsy zdezaktywowane
httpsApp.use((req, res, next) => {
  const allowedOrigins = [
    "https://localhost:8080",
    "https://localhost:8081",
    "https://fantastic-app.mac.pl:8080",
    "https://fantastic-app.mac.pl:8081",
  ];
  const origin = req.headers.origin;
  if (allowedOrigins.includes(origin)) {
    res.setHeader("Access-Control-Allow-Origin", origin);
  }
  res.header(
    "Access-Control-Allow-Methods",
    "GET, OPTIONS, POST, PATCH, DELETE"
  );
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
  res.header("Access-Control-Allow-Credentials", true);
  return next();
});

/* u mnie nie działało */
// const allowlist = [
//   "https://localhost:8080",
//   "https://localhost:8081",
//   "https://fantastic-app.mac.pl:8080",
//   "https://fantastic-app.mac.pl:8081",
//   "https://fantastic-app.mac.pl:8080/uczonko",
//   "https://fantastic-app.mac.pl:8081/uczonko",
// ];
// var corsOptionsDelegate = function (req, callback) {
//   var corsOptions;
//   if (allowlist.indexOf(req.header("Origin")) !== -1) {
//     corsOptions = { origin: true }; // reflect (enable) the requested origin in the CORS response
//   } else {
//     corsOptions = { origin: false }; // disable CORS for this request
//   }
//   callback(null, corsOptions); // callback expects two parameters: error and options
// };

let db;
// const connecting = setInterval(test, 3000);

const connection = connectToDb((err) => {
  if (!err) {
    console.log(`we're in ;)`);
    db = getDb();
    // console.log(db);
  } else if (err) {
    console.log("No such collection or database is down, ", err);
  }
});

const httpsOptions = {
  cert: fs.readFileSync(path.join(__dirname, "ssl", "tls.crt")),
  key: fs.readFileSync(path.join(__dirname, "ssl", "tls.key")),
};

const httpServer = http.createServer(httpApp);
const httpsServer = https.createServer(httpsOptions, httpsApp);

//port 4999
// httpApp.get("*", cors(corsOptionsDelegate), (req, res) => {
//   res.redirect(304, "https://fantastic-api.mac.pl:5000");
// });
httpApp.get("*", (req, res) => {
  res.redirect(301, "https://fantastic-api.mac.pl:5000");
});

//port 5000
httpsApp.get("/", (req, res) => {
  res.status(200).send("siemanooooo");
});
httpsApp.get("/allResources", (req, res) => {
  // console.log("allResources", db);
  let all = [];
  // console.log(db.collection("cokolwiek").find());
  db.collection("uczonko")
    .find()
    .forEach((element) => {
      // console.log(element);
      all.push(element);
    })
    .then(() => res.status(200).json(all))
    .catch((err) =>
      res.status(500).json({ err: "Eror w get'cie /allResources: ", err })
    );
});

httpsApp.post("/resource", (req, res) => {
  const sth = req.body;
  // console.log("req: ", req);
  // console.log("req.body: ", req.body);

  db.collection("uczonko")
    .insertOne(sth)
    .then((result) => {
      res.status(201).json(result);
    })
    .catch((err) =>
      res.status(500).json({ err: "Could not create a new record: ", err })
    );
});
httpsApp.delete("/resource/:id", (req, res, next) => {
  db.collection("uczonko")
    // .deleteOne({ id: "sdawdasdsdawdasd" })
    .deleteOne({ id: req.params.id })
    .then((result) => {
      console.log(typeof result.deletedCount);
      console.log(!result.deletedCount);
      if (!result.deletedCount) {
        if (!!id) {
          console.log("Cannot find this element: ", result);
          res.status(404).json({ err: "Cannot find this element: ", result });
          next();
        }
        console.log("Cannot delete this element: ", result);
        res.status(400).json({ err: "Cannot delete this element: ", result });
        next();
      } else {
        res.status(202).json(result);
        next();
      }
    })
    .catch((err) => {
      console.log("Fatal Error while deleting: ", err);
      res.status(500).json({ err: "Fatal Error while deleting", err });
      next();
    });
});
httpsApp.patch("/resource/:id", (req, res, next) => {
  // console.log("edycja tego, w api:", req.body);
  const updates = req.body;
  db.collection("uczonko")
    // .updateOne({ id: undefined }, { $set: updates })
    .updateOne({ id: req.params.id }, { $set: updates })
    .then((result) => {
      if (!result.matchedCount) {
        console.log("Cannot find element with this id: ", result);
        res
          .status(404)
          .json({ err: "Cannot find element with this id: ", result });
        next();
      } else if (!result.modifiedCount) {
        console.log("Nothing has changed: ", result);
        res.status(204).json({ err: "Nothing has changed: ", result });
        next();
      } else {
        console.log("Everything is fine. result: ", result);
        res.status(200).json(result);
        next();
      }
    })
    .catch((err) => {
      console.log("Fatal Error while updateing: ", err);
      res.status(500).json({ err: "Fatal Error while updateing: ", err });
      next();
    });
});

// testy testy testy testy testy testy testy testy testy testy testy testy testy testy
// console.log("connection", connection);
// console.log(typeof connection);
// let counter = 0;
// const interval = setInterval(() => {
//   counter++;
//   console.log(`test ${counter}`);
//   if (counter >= 3) {
//     clearInterval(interval);
//     counter = 0;
//     console.log("koniec");
//   }
// }, 1000);
httpsServer.listen(HTTPS_PORT, () => {
  console.log(`HTTPS Server is listening on portorico ${HTTPS_PORT}`);
});
httpServer.listen(HTTP_PORT, () => {
  console.log(`HTTP Server running on port ${HTTP_PORT}`);
});
